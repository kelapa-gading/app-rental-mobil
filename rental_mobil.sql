-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.2.3-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for rental_mobil
CREATE DATABASE IF NOT EXISTS `rental_mobil` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rental_mobil`;

-- Dumping structure for table rental_mobil.admin
CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `nama_admin` varchar(120) NOT NULL,
  `username` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `no_telp` int(50) DEFAULT NULL,
  `no_rekening` int(25) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_admin`),
  UNIQUE KEY `nama_admin` (`nama_admin`,`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.
-- Dumping structure for table rental_mobil.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `id_customer` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(120) NOT NULL,
  `username` varchar(120) NOT NULL,
  `alamat` varchar(120) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `no_ktp` int(25) NOT NULL,
  `password` varchar(120) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_customer`),
  UNIQUE KEY `nama` (`nama`,`username`,`no_telp`,`no_ktp`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.
-- Dumping structure for table rental_mobil.mobil
CREATE TABLE IF NOT EXISTS `mobil` (
  `id_mobil` int(11) NOT NULL AUTO_INCREMENT,
  `kode_tipe` varchar(120) NOT NULL,
  `merk` varchar(120) NOT NULL,
  `no_plat` varchar(20) NOT NULL,
  `warna` varchar(20) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `status` varchar(50) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `denda` int(11) DEFAULT NULL,
  `air_cond` int(11) DEFAULT NULL,
  `supir` int(11) DEFAULT NULL,
  `tujuh_kursi` int(11) DEFAULT NULL,
  `sport_mode` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_mobil`),
  UNIQUE KEY `kode_tipe` (`kode_tipe`,`merk`,`no_plat`,`tahun`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.
-- Dumping structure for table rental_mobil.rental
CREATE TABLE IF NOT EXISTS `rental` (
  `id_rental` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) NOT NULL,
  `tanggal_rental` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `tanggal_pengembalian` date NOT NULL,
  `status_rental` varchar(50) NOT NULL,
  `status_pengembalian` varchar(50) NOT NULL,
  PRIMARY KEY (`id_rental`),
  UNIQUE KEY `id_customer` (`id_customer`,`tanggal_pengembalian`,`status_rental`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.
-- Dumping structure for table rental_mobil.tipe
CREATE TABLE IF NOT EXISTS `tipe` (
  `id_tipe` int(11) NOT NULL AUTO_INCREMENT,
  `kode_tipe` varchar(10) NOT NULL,
  `nama_tipe` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tipe`),
  UNIQUE KEY `kode_tipe` (`kode_tipe`,`nama_tipe`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.
-- Dumping structure for table rental_mobil.transaksi
CREATE TABLE IF NOT EXISTS `transaksi` (
  `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) NOT NULL,
  `id_mobil` int(11) NOT NULL,
  `tanggal_sewa` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `harga` varchar(120) NOT NULL,
  `denda` varchar(120) NOT NULL,
  `total_denda` varchar(120) NOT NULL,
  `tanggal_pengembalian` date NOT NULL,
  `status_pengembalian` varchar(50) NOT NULL,
  `status_rental` varchar(50) NOT NULL,
  `bukti_pembayaran` varchar(120) NOT NULL,
  `status_pembayaran` varchar(120) NOT NULL,
  PRIMARY KEY (`id_transaksi`),
  UNIQUE KEY `id_customer` (`id_customer`,`id_mobil`,`tanggal_sewa`,`tanggal_pengembalian`,`status_pengembalian`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
