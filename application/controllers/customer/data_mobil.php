<?php
class Data_mobil extends CI_Controller
{
    public function index()
    {

        // $data['mobil'] = $this->rent_model->get_data()->result();
        $data['customer']  = $this->rent_model->ambilData('customer')->result();

        $config['base_url']     = 'http://localhost/Rental_Autonet/customer/data_mobil/index';
        $config['total_rows']   = $this->db->count_all('mobil');
        $config['per_page']     = 5;
        $config['uri_segment']  = 4;
        $choice                 = $config['total_rows'] / $config['per_page'];
        $config['num_links']    = floor($choice);
        $config['first_link']    = 'First';
        $config['last_link']     = 'Last';
        $config['last_link']     = 'Last';
        $config['next_link']     = 'Next';
        $config['prev_link']     = 'Previous';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';







        $this->pagination->initialize($config);
        $data['page']           = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $data['mobil'] = $this->rent_model->get_data($config['per_page'], $data['page'])->result();
        $data['pagination']     = $this->pagination->create_links();
        $this->load->view('templates_customer/header');
        $this->load->view('customer/data_mobil', $data);
        $this->load->view('templates_customer/footer');
    }
    public function detail_mobil($id)
    {
        $data['detail'] = $this->rent_model->ambil_id_mobil($id);
        $this->load->view('templates_customer/header');
        $this->load->view('customer/detail_mobil', $data);
        $this->load->view('templates_customer/footer');
    }
}
