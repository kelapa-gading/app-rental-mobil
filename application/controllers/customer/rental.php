<?php
class Rental extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!isset($this->session->userdata['username'])) {
            $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
					Maaf Anda Belum Login
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				  </div>');
            redirect('auth/login');
        }
    }
    public function tambah_rental($id)
    {
        $data['detail'] = $this->rent_model->ambil_id_mobil($id);
        $this->load->view('templates_customer/header');
        $this->load->view('customer/tambah_rental', $data);
        $this->load->view('templates_customer/footer');
    }

    public function aksi_tambah_rental()
    {
        $id_customer         = $this->session->userdata('id_customer');
        $id_mobil            = $this->input->post('id_mobil');
        $tanggal_sewa        = $this->input->post('tanggal_sewa');
        $tanggal_kembali     = $this->input->post('tanggal_kembali');
        $denda               = $this->input->post('denda');
        $harga               = $this->input->post('harga');
        $jumlah               = $this->input->post('Jumlah');

        $data = array(
            'id_customer'       => $id_customer,
            'id_mobil'          => $id_mobil,
            'tanggal_sewa'      => $tanggal_sewa,
            'tanggal_kembali'   => $tanggal_kembali,
            'harga'             => $harga,
            'denda'             => $denda,
            'Jumlah'             => $jumlah,
            'tanggal_pengembalian' => '-',
            'status_rental'         => 'Belum Selesai',
            'status_pengembalian'   => 'Belum_Kembali',
            'total_denda'           => '0'
        );

        $this->rent_model->insert_data($data, 'transaksi');

        $status = array(
            'status' => '0'
        );

        $id     = array(
            'id_mobil' => $id_mobil
        );
        $this->rent_model->update_data('mobil', $status, $id);
        $this->session->set_flashdata('pesan', '<div class="alert alert-warning alert-dismissible fade show" role="alert">
           !Transaksi Berhasil,Silahkan Menuju Halaman Transaksi untuk melakukan pembayaran!.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>');
        redirect('customer/data_mobil');
    }
}
