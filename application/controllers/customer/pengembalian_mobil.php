<?php

class Pengembalian_mobil extends CI_Controller
{
    public function index()
    {
        $customer = $this->session->userdata('id_customer');
        $data['transaksi'] = $this->db->query("SELECT * FROM transaksi tr, mobil mb , customer cs WHERE tr.id_mobil = mb.id_mobil AND tr.id_customer = cs.id_customer AND cs.id_customer='$customer' ORDER BY id_transaksi DESC")->result();
        $this->load->view('templates_customer/header');
        $this->load->view('customer/pengembalian', $data);
        $this->load->view('templates_customer/footer');
    }

    public function aksi_pengembalian_mobil()
    {
        $id                     = $this->input->post('id_transaksi');
        $tanggal_pengembalian   = $this->input->post('tanggal_pengembalian');
        $status_rental          = $this->input->post('status_rental');
        $status_pengembalian    = $this->input->post('status_pengembalian');
        $tanggal_kembali        = $this->input->post('tanggal_kembali');
        $denda                  = $this->input->post('denda');
        $jumlah                 = $this->input->post('Jumlah');

        $x                 = strtotime($tanggal_pengembalian);
        $y                 = strtotime($tanggal_kembali);
        $selisih           = abs($x - $y) / (60 * 60 * 24);
        $total_denda       = $selisih * $denda * $jumlah;

        $data = array(
            'tanggal_pengembalian' => $tanggal_pengembalian,
            'status_rental'        => $status_rental,
            'status_pengembalian'  => $status_pengembalian,
            'total_denda'           => $total_denda,
            'Jumlah'                => $jumlah
        );

        $where = array(
            'id_transaksi' => $id
        );

        $this->rent_model->update_data('transaksi', $data, $where);
        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Mobil Dikembalikan, Mohon dibayarkan denda sebesar Rp.' . number_format($total_denda, 0, '.', ',') . " Terima Kasih<i class='fa fa-smile'></i>" . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>');
        redirect('customer/transaksi');
    }
}
