<?php

class auth extends CI_Controller
{

    public function login()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates_customer/header');
            $this->load->view('form_login');
            $this->load->view('templates_customer/footer');
        } else {
            $username = $this->input->post('username');
            $password = MD5($this->input->post('password'));

            $cek = $this->rent_model->cek_login($username, $password);

            if ($cek == FALSE) {
                $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                Username or Password not mathcing!.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>');
                redirect('auth/login');
            } else {
                $this->session->set_userdata('id_customer', $cek->id_customer);
                $this->session->set_userdata('id_admin', $cek->id_admin);
                $this->session->set_userdata('username', $cek->username);
                $this->session->set_userdata('role_id', $cek->role_id);
                $this->session->set_userdata('nama', $cek->nama);
                $this->session->set_userdata('nama_admin', $cek->nama_admin);

                switch ($cek->role_id) {
                    case 1:
                        redirect('admin/dashboard');
                        break;
                    case 2:
                        redirect('customer/dashboard');
                        break;
                }
            }
        }
    }
    public function _rules()
    {
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('customer/dashboard');
    }

    public function ganti_password()
    {
        $this->load->view('templates_admin/header');
        $this->load->view('change_password');
        $this->load->view('templates_admin/footer');
    }
    public function aksi_ganti_password()
    {
        $pass_baru = $this->input->post('pass_baru');
        $ulangi_pass = $this->input->post('ulangi_pass');

        $this->form_validation->set_rules('pass_baru', 'Password yang Baru', 'required|matches[ulangi_pass]');
        $this->form_validation->set_rules('ulangi_pass', 'Ulangi Passowrd', 'required');

        if ($this->form_validation->run() != false) {
            $data   = array('password' => md5($pass_baru));
            $id     = array('id_customer' => $this->session->userdata('id_customer'));
            $this->rent_model->update_password($id, $data, 'customer');
            $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                Your Password Has Been Changed,please will login!.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>');
            redirect('auth/login');
        } else {
            $this->load->view('templates_admin/header');
            $this->load->view('ganti_password');
            $this->load->view('templates_admin/footer');
        }
    }
}
