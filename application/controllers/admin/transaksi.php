<?php

class Transaksi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!isset($this->session->userdata['username'])) {
            $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    Maaf Login Dulu sebagai Admin
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        $data['transaksi'] = $this->db->query("SELECT * FROM transaksi tr, mobil mb , customer cs WHERE tr.id_mobil = mb.id_mobil AND tr.id_customer = cs.id_customer")->result();
        $this->load->view('templates_admin/header');
        $this->load->view('templates_admin/sidebar');
        $this->load->view('admin/data_transaksi', $data);
        $this->load->view('templates_admin/footer');
    }

    public function pembayaran($id)
    {
        $where = array('id_transaksi' => $id);
        $data['pembayaran'] = $this->db->query("SELECT * FROM transaksi WHERE id_transaksi ='$id'")->result();
        $this->load->view('templates_admin/header');
        $this->load->view('templates_admin/sidebar');
        $this->load->view('admin/konfirmasi_pembayaran', $data);
        $this->load->view('templates_admin/footer');
    }
    public function cek_pembayaran()
    {
        $id = $this->input->post('id_transaksi');
        // $status_pembayaran = $this->input->post('status_pembayaran');
        $status_pembayaran = 1;

        $data = array('status_pembayaran' => $status_pembayaran,);
        $where = array('id_transaksi' => $id);

        $this->rent_model->update_data('transaksi', $data, $where);
        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Cek Pembayaran Di Update!.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>');
        redirect('admin/transaksi');
    }

    public function download_pembayaran($id)
    {
        $this->load->helper('download');
        $filePembayaran = $this->rent_model->downloadPembayaran($id);

        $file = 'assets/upload/' . $filePembayaran['bukti_pembayaran'];

        force_download($file, NULL);
    }

    public function transaksi_selesai($id)
    {
        $where = array('id_transaksi' => $id);
        $data['transaksi'] = $this->db->query("SELECT * FROM transaksi WHERE id_transaksi = '$id'")->result();
        $this->load->view('templates_admin/header');
        $this->load->view('templates_admin/sidebar');
        $this->load->view('admin/transaksi_selesai', $data);
        $this->load->view('templates_admin/footer');
    }

    public function aksi_transaksi_selesai()
    {
        $id                     = $this->input->post('id_transaksi');
        $tanggal_pengembalian   = $this->input->post('tanggal_pengembalian');
        $status_rental          = $this->input->post('status_rental');
        $status_pengembalian    = $this->input->post('status_pengembalian');
        $tanggal_kembali        = $this->input->post('tanggal_kembali');
        $denda                  = $this->input->post('denda');
        $jumlah                 = $this->input->post('Jumlah');

        $x                 = strtotime($tanggal_pengembalian);
        $y                 = strtotime($tanggal_kembali);
        $selisih           = abs($x - $y) / (60 * 60 * 24);
        $total_denda       = $selisih * $denda * $jumlah;
        $data = array(
            'tanggal_pengembalian' => $tanggal_pengembalian,
            'status_rental'        => $status_rental,
            'status_pengembalian'  => $status_pengembalian,
            'total_denda'           => $total_denda,
            'Jumlah'                => $jumlah
        );

        $where = array(
            'id_transaksi' => $id
        );

        $this->rent_model->update_data('transaksi', $data, $where);
        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Transaksi Berhasil Diupdate!.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>');
        redirect('admin/transaksi');
    }

    public function transaksi_batal($id)
    {
        $where = array('id_transaksi' => $id);
        $data = $this->rent_model->get_where($where, 'transaksi')->row();

        $where2 = array('id_mobil' => $data->id_mobil);
        $data2   = array('status' => '1');

        $this->rent_model->update_data('mobil', $data2, $where2);
        $this->rent_model->delete_data($where, 'transaksi');
        $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            Transaksi Berhasil Batal!.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>');
        redirect('admin/transaksi');
    }
}
