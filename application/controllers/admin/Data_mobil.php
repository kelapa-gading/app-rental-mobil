        <?php

        class Data_mobil extends CI_Controller
        {
            function __construct()
            {
                parent::__construct();
                if (!isset($this->session->userdata['username'])) {
                    $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                    Maaf Login Dulu sebagai Admin
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>');
                    redirect('auth/login');
                }
            }

            public function index()
            {

                $config['base_url'] = "http://localhost/Rental_Autonet/admin/data_mobil/index";
                $config['total_rows'] = $this->db->count_all('mobil');
                $config['per_page']   = 5;
                $config['uri_segment']  = 4;
                $choice                 = $config['total_rows'] / $config['per_page'];
                $config['num_links']    = floor($choice);
                $config['first_link']    = 'First';
                $config['last_link']     = 'Last';
                $config['next_link']     = 'Next';
                $config['prev_link']     = 'Previous';
                $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
                $config['full_tag_close']   = '</ul></nav></div>';
                $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
                $config['num_tag_close']    = '</span></li>';
                $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
                $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
                $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
                $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
                $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
                $config['prev_tagl_close']  = '</span>Next</li>';
                $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
                $config['first_tagl_close'] = '</span></li>';
                $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
                $config['last_tagl_close']  = '</span></li>';






                $this->pagination->initialize($config);
                $data['page']           = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

                $data['mobil'] = $this->rent_model->get_data($config['per_page'], $data['page'])->result();
                $data['pagination']     = $this->pagination->create_links();
                $data['tipe'] = $this->rent_model->ambilData('tipe')->result();
                $this->load->view('templates_admin/header');
                $this->load->view('templates_admin/sidebar');
                $this->load->view('admin/data_mobil', $data);
                $this->load->view('templates_admin/footer');
            }
            public function tambah_mobil()
            {
                $data['tipe'] = $this->rent_model->ambilData('tipe')->result();
                $this->load->view('templates_admin/header');
                $this->load->view('templates_admin/sidebar');
                $this->load->view('admin/form_tambah_mobil', $data);
                $this->load->view('templates_admin/footer');
            }

            public function aksi_tambah_mobil()
            {
                $this->_rules();
                if ($this->form_validation->run() == FALSE) {
                    $this->tambah_mobil();
                } else {
                    $kode_tipe          = $this->input->post('kode_tipe');
                    $merk               = $this->input->post('merk');
                    $no_plat            = $this->input->post('no_plat');
                    $warna              = $this->input->post('warna');
                    $tahun              = $this->input->post('tahun');
                    $status             = $this->input->post('status');
                    $harga              = $this->input->post('harga');
                    $denda              = $this->input->post('denda');
                    $fast_charging      = $this->input->post('fast_charging');
                    $free_gass          = $this->input->post('free_gass');
                    $tujuh_kursi        = $this->input->post('tujuh_kursi');
                    $matic              = $this->input->post('matic');
                    $gambar             = $_FILES['gambar']['name'];
                    if ($gambar = '') {
                    } else {
                        $config['upload_path']     = './assets/upload';
                        $config['allowed_types']   = 'jpg|jpeg|png|tiff';

                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('gambar')) {
                            echo "Gambar Tidak Berhasil Diupload";
                        } else {
                            $gambar = $this->upload->data('file_name');
                        }
                    }
                    $data = array(
                        'kode_tipe'      => $kode_tipe,
                        'merk'           => $merk,
                        'no_plat'        => $no_plat,
                        'tahun'          => $tahun,
                        'warna'          => $warna,
                        'status'         => $status,
                        'harga'          => $harga,
                        'denda'          => $denda,
                        'fast_charging'  => $fast_charging,
                        'free_gass'      => $free_gass,
                        'tujuh_kursi'    => $tujuh_kursi,
                        'matic'          => $matic,
                        'gambar'         => $gambar
                    );
                    $this->rent_model->insert_data($data, 'mobil');
                    $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mobil Berhasil Ditambahkan!.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>');
                    redirect('admin/data_mobil');
                }
            }

            public function update_mobil($id)
            {
                $where = array('id_mobil' => $id);
                $data['mobil'] = $this->db->query("SELECT * FROM mobil mb, tipe tp WHERE mb.kode_tipe=tp.kode_tipe AND mb.id_mobil = '$id'")->result();
                $data['tipe'] = $this->rent_model->ambilData('tipe')->result();

                $this->load->view('templates_admin/header');
                $this->load->view('templates_admin/sidebar');
                $this->load->view('admin/form_update_mobil', $data);
                $this->load->view('templates_admin/footer');
            }
            public function update_mobil_aksi()
            {
                $this->_rules();

                if ($this->form_validation->run() == FALSE) {
                    $this->update_mobil($id = "id_mobil");
                } else {
                    $id                 = $this->input->post('id_mobil');
                    $kode_tipe          = $this->input->post('kode_tipe');
                    $merk               = $this->input->post('merk');
                    $warna              = $this->input->post('warna');
                    $no_plat            = $this->input->post('no_plat');
                    $tahun              = $this->input->post('tahun');
                    $status             = $this->input->post('status');
                    $harga              = $this->input->post('harga');
                    $denda              = $this->input->post('denda');
                    $fast_charging      = $this->input->post('fast_charging');
                    $free_gass          = $this->input->post('free_gass');
                    $tujuh_kursi        = $this->input->post('tujuh_kursi');
                    $matic              = $this->input->post('matic');
                    $gambar             = $_FILES['gambar']['name'];
                    if ($gambar) {
                        $config['upload_path']     = './assets/upload';
                        $config['allowed_types']   = 'jpg|jpeg|png|tiff';

                        $this->load->library('upload', $config);

                        if ($this->upload->do_upload('gambar')) {
                            $gambar = $this->upload->data('file_name');
                            $this->db->set('gambar', $gambar);
                        } else {
                            echo $this->upload->display_errors();
                        }
                    }
                    $data = array(
                        'kode_tipe'      => $kode_tipe,
                        'merk'           => $merk,
                        'no_plat'        => $no_plat,
                        'warna'          => $warna,
                        'tahun'          => $tahun,
                        'status'         => $status,
                        'harga'          => $harga,
                        'denda'          => $denda,
                        'fast_charging'  => $fast_charging,
                        'free_gass'      => $free_gass,
                        'tujuh_kursi'    => $tujuh_kursi,
                        'matic'          => $matic,
                    );

                    $where = array(
                        'id_mobil' => $id
                    );
                    $this->rent_model->update_data('mobil', $data, $where);
                    $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Mobil Berhasil Diupdate!.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>');
                    redirect('admin/data_mobil');
                }
            }

            public function _rules()
            {
                $this->form_validation->set_rules('kode_tipe', 'Kode tipe', 'required');
                $this->form_validation->set_rules('merk', 'Merk', 'required');
                $this->form_validation->set_rules('no_plat', 'No Plat', 'required');
                $this->form_validation->set_rules('tahun', 'Tahun', 'required');
                $this->form_validation->set_rules('warna', 'Warna', 'required');
                $this->form_validation->set_rules('status', 'Status', 'required');
                $this->form_validation->set_rules('harga', 'Harga', 'required');
                $this->form_validation->set_rules('denda', 'Denda', 'required');
                $this->form_validation->set_rules('fast_charging', 'Fast Charge', 'required');
                $this->form_validation->set_rules('free_gass', 'Free Battery', 'required');
                $this->form_validation->set_rules('tujuh_kursi', 'Seven Seater', 'required');
                $this->form_validation->set_rules('matic', 'Automatic', 'required');
            }
            public function detail_mobil($id)
            {
                $data['detail'] =  $this->rent_model->ambil_id_mobil($id);

                $this->load->view('templates_admin/header');
                $this->load->view('templates_admin/sidebar');
                $this->load->view('admin/detail_mobil', $data);
                $this->load->view('templates_admin/footer');
            }
            public function delete_mobil($id)
            {
                $where = array('id_mobil' => $id);

                $this->rent_model->delete_data($where, 'mobil');
                $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            Data Mobil Berhasil Dihapus!.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>');

                redirect('admin/data_mobil');
            }
        }
        ?>