<body class="loader-active">

  <!--== Preloader Area Start ==-->
  <div class="preloader">
    <div class="preloader-spinner">
      <div class="loader-content">
        <img src="<?= base_url('../img/preloader.gif') ?>" alt="JSOFT">
      </div>
    </div>
  </div>
  <!--== Preloader Area End ==-->

  <!--== Page Title Area Start ==-->
  <section id="page-title-area" class="section-padding overlay">
    <div class="container">
      <div class="row">
        <!-- Page Title Start -->
        <div class="col-lg-12">
          <div class="section-title  text-center">
            <h2>Halaman Mendaftar</h2>
            <span class="title-line"><i class="fa fa-car"></i></span>
            <p>Sebelum Kamu Mengunjungi atau Rental Tolong Mendaftar,Terima Kasih (:</p>
          </div>
        </div>
        <!-- Page Title End -->
      </div>
    </div>
  </section>
  <!--== Page Title Area End ==-->

  <!--== Login Page Content Start ==-->
  <section id="lgoin-page-wrap" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-8 m-auto">
          <div class="login-page-content">
            <div class="login-form">
              <h3>Hai Apa Kabarmu Hari Ini? <strong>Daftarin data kamu dulu ya <i class="far fa-smile-wink"></i></strong></h3>
              <form method="POST" action="<?= base_url('register') ?>" class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <div class="nama">
                      <input type="text" placeholder="Masukan Nama Lengkap Kamu" name="nama">
                    </div>
                    <div class="username">
                      <input type="text" placeholder="Username Kamu" name="username">
                    </div>
                    <div class="alamat">
                      <input type="text" placeholder="alamat Lengkap Kamu" name="alamat">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="no_telp">
                      <input type="number" placeholder="No Handphone Yang kamu Punya" name="no_telp">
                    </div>

                    <div class="no_ktp">
                      <input type="text" placeholder="Nomor Identitas Atau KTP" name="no_ktp">
                    </div>

                    <div class="password">
                      <input type="password" placeholder="Password" name="password">
                      <input type="hidden" name="role_id" value="2">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <select name="gender" class="form-control">
                    <option id="option">Jenis Kelamin</option>
                    <option value="laki-laki" id="option">Laki-laki</option>
                    <option value="perempuan" id="option">Perempuan</option>
                  </select>
                </div>


                <div class="log-btn">
                  <button type="submit"><i class="fa fa-check-square"></i> Mendaftar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== Login Page Content End ==-->

  <!--== Footer Area Start ==-->
  <section id="footer-area">
    <!-- Footer Widget Start -->
    <div class="footer-widget-area">
      <div class="container">
        <div class="row">
          <!-- Single Footer Widget Start -->
          <div class="col-lg-4 col-md-6">
            <div class="single-footer-widget">
              <h2>Tentang Kita</h2>
              <div class="widget-body">
                <img src="<?php echo base_url() ?>assets/assets_shop/img/logo.jpeg" alt="JSOFT" width="200px">
                <p>Rental Autonet merupakan usaha perseorangan yang bergerak di bidang otomotif dalam pelayanan sewa mobil electric.</p>
              </div>
            </div>
          </div>
          <!-- Single Footer Widget End -->
        </div>
      </div>
    </div>