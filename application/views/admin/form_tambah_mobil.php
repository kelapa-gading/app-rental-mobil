<div class="main-content">
    <section class="section">
        <div class="section-header bg-dark">
            <h1>Form Input Data mobil</h1>
        </div>
        <div class="card">
            <div class="card-body bg-dark">
                <div class="row">
                    <form method="POST" action="<?php echo base_url('admin/data_mobil/aksi_tambah_mobil') ?>" enctype="multipart/form-data">
                        <div class="col-md6">
                            <div class="form-group mb-n1">
                                <label class="text-primary">Tipe Mobil</label>
                                <select name="kode_tipe" class="form-control">
                                    <option value="">***Pilih Tipe Mobilmu**</option>
                                    <?php foreach ($tipe as $tp) : ?>
                                        <option value="<?php echo $tp->kode_tipe ?>">
                                            <?php echo $tp->nama_tipe ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo form_error('kode_tipe', '<div class="text-small text-danger">', '</div>') ?>
                            </div>
                            <div class="form-group mb-n2">
                                <label class="text-primary">Merk Mobil</label>
                                <br />
                                <input type="text" name="merk" class="form-control">
                                <?php echo form_error('merk', '<div class="text-small text-danger">', '</div>') ?>
                            </div>

                            <div class="form-group mb-n2 mt-2">
                                <label class="text-primary">No. Plat</label>
                                <br />
                                <input type="text" name="no_plat" class="form-control">
                                <?php echo form_error('no_plat', '<div class="text-small text-danger">', '</div>') ?>
                            </div>

                            <div class="form-group mb-n2 mt-2">
                                <label class="text-primary">Warna</label>
                                <br />
                                <input type="text" name="warna" class="form-control">
                                <?php echo form_error('warna', '<div class="text-small text-danger">', '</div>') ?>
                            </div>

                            <div class="form-group mt-2 mb-n1">
                                <label class="text-primary">Fast Charging</label>
                                <select name="fast_charging" class="form-control">
                                    <option value="1">Tersedia</option>
                                    <option value="0">Tidak Tersedia</option>
                                </select>
                            </div>
                            <?php echo form_error('fast_charging', '<div class="text-small text-danger">', '</div>') ?>
                            <div class="form-group mb-n1 mt-2">
                                <label class="text-primary">Free Battery Full</label>
                                <select name="free_gass" class="form-control">
                                    <option value="1">Tersedia</option>
                                    <option value="0">Tidak Tersedia</option>
                                </select>
                            </div>
                            <?php echo form_error('free_gass', '<div class="text-small text-danger">', '</div>') ?>
                            <div class="form-group mt-2 mb-n1">
                                <label class="text-primary">7_Seater</label>
                                <select name="tujuh_kursi" class="form-control">
                                    <option value="1">Tersedia</option>
                                    <option value="0">Tidak Tersedia</option>
                                </select>
                            </div>

                            <?php echo form_error('tujuh_kursi', '<div class="text-small text-danger">', '</div>') ?>
                            <div class="form-group mb-n1">
                                <label class="text-primary">Transmision matic</label>
                                <select name="matic" class="form-control">
                                    <option value="1">Tersedia</option>
                                    <option value="0">Tidak Tersedia</option>
                                </select>
                            </div>

                            <?php echo form_error('matic', '<div class="text-small text-danger">', '</div>') ?>

                            <div class="form-group mb-n2">
                                <label class="text-primary">Harga Sewa/Hari</label>
                                <br />
                                <input type="number" name="harga" class="form-control">
                                <?php echo form_error('harga', '<div class="text-small text-danger">', '</div>') ?>
                            </div>

                            <div class="form-group mb-n2 mt-2">
                                <label class="text-primary">Denda</label>
                                <br />
                                <input type="number" name="denda" class="form-control">
                                <?php echo form_error('denda', '<div class="text-small text-danger">', '</div>') ?>
                            </div>

                            <div class="form-group mb-n2 mt-2">
                                <label class="text-primary">Tahun Pembuatan</label>
                                <br />
                                <input type="text" name="tahun" class="form-control">
                                <?php echo form_error('tahun', '<div class="text-small text-danger">', '</div>') ?>
                            </div>

                            <div class="form-group mt-2">
                                <label class="text-primary">Status</label>
                                <br />
                                <select name="status" class="form-control">
                                    <option value="">**Status Sekarang**</option>
                                    <option value="1">Tersedia</option>
                                    <option value="0">Tidak Tersedia</option>
                                </select>
                            </div>
                            <?php echo form_error('status', '<div class="text-small text-danger">', '</div>') ?>
                            <div class="form-group mb-n2">
                                <label class="text-primary">Gambar</label>
                                <br />
                                <input type="file" name="gambar" class="form-control">
                            </div>
                            <div class=" mb-3 pt-2">
                                <button type="submit" class="btn btn-primary mr-1">Simpan</button>
                                <button type="reset" class="btn btn-danger mt-1  ml-2">Hapus</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </section>
</div>