<div class="main-content">
    <section class="section">
        <div class="section-header bg-dark">
            <h1>Data Customer</h1>
        </div>
    </section>
    <form method="POST" action="<?php echo base_url('admin/data_customer/aksi_tambah_customer') ?>">
        <div class="form-group">
            <label class="text-primary">Nama</label>
            <input type="text" name="nama" class="form-control">
            <?php echo form_error('nama', '<span class="text-small text-danger">', '</span>') ?>
        </div>
        <div class="form-group">
            <label class="text-primary">Username</label>
            <input type="text" name="username" class="form-control">
            <?php echo form_error('username', '<span class="text-small text-danger">', '</span>') ?>
        </div>
        <div class="form-group">
            <label class="text-primary">Alamat</label>
            <input type="text" name="alamat" class="form-control">
            <?php echo form_error('alamat', '<span class="text-small text-danger">', '</span>') ?>
        </div>
        <div class="form-group">
            <label class="text-primary">Gender</label>
            <select class='form-control' name="gender">
                <option value="gender">--Pilih Gender--</option>
                <option value="laki-laki">Laki-laki</option>
                <option value="perempuan">Perempuan</option>
            </select>
            <?php echo form_error('gender', '<span class="text-small text-danger">', '</span>') ?>
        </div>
        <div class="form-group">
            <label class="text-primary">No. Telepon</label>
            <input type="text" name="no_telp" class="form-control">
            <?php echo form_error('no_telp', '<span class="text-small text-danger">', '</span>') ?>
        </div>
        <div class="form-group">
            <label class="text-primary">No. KTP</label>
            <input type="text" name="no_ktp" class="form-control">
            <?php echo form_error('no_ktp', '<span class="text-small text-danger">', '</span>') ?>
        </div>
        <div class="form-group">
            <label class="text-primary">Password</label>
            <input type="password" name="password" class="form-control">
            <?php echo form_error('password', '<span class="text-small text-danger">', '</span>') ?>
        </div>

        <button type="submit" class="btn btn-primary mr-1">Submit</button>
        <button type="reset" class="btn btn-danger ml-1">Reset</button>
    </form>
</div>