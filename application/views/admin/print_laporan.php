<h2 class="text-center">Laporan Transaksi Rental Autonet</h2>
<table>
    <tr>
        <td>Dari Tanggal</td>
        <td>:</td>
        <td><?php echo date('d-M-Y', strtotime($_GET['dari'])); ?></td>
    </tr>
    <tr>
        <td>Sampai Tanggal</td>
        <td>:</td>
        <td><?php echo date('d-M-Y', strtotime($_GET['sampai'])); ?></td>
    </tr>
</table>

<table class="table table-bordered table-striped mt-4 table-dark">

    <tr>
        <th>No</th>
        <th>Customer</th>
        <th>Mobil</th>
        <th>Tanggal Sewa</th>
        <th>Tanggal Kembali</th>
        <th>Harga Sewa/Hari</th>
        <th>Denda/Hari</th>
        <th>Total Denda</th>
        <th>Tanggal Pengembalian</th>
        <th>Status Pengembalian</th>
        <th>Status rental</th>
    </tr>

    <?php
    $no = 1;
    foreach ($laporan as $tr) : ?>
        <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $tr->nama ?></td>
            <td><?php echo $tr->merk ?></td>
            <td><?php echo date('d/m/Y', strtotime($tr->tanggal_sewa)); ?></td>
            <td><?php echo date('d/m/Y', strtotime($tr->tanggal_kembali)); ?></td>
            <td>Rp. <?php echo number_format($tr->harga, 0, ',', '.') ?></td>
            <td>Rp. <?php echo number_format($tr->denda, 0, ',', '.') ?></td>
            <td>Rp. <?php echo number_format($tr->total_denda, 0, ',', '.') ?></td>
            <td>
                <?php if ($tr->tanggal_pengembalian == "0000-00-00") {
                    echo "_";
                } else {
                    echo date('d/m/Y', strtotime($tr->tanggal_pengembalian));
                }
                ?>
            </td>
            <td><?php if ($tr->status_pengembalian == "Kembali") {
                    echo "Kembali";
                } else {
                    echo "Belum Kembali";
                } ?></td>
            <td><?php if ($tr->status_rental == "Sudah Selesai") {
                    echo "Sudah Selesai";
                } else {
                    echo "Belum Selesai";
                } ?></td>
        </tr>
    <?php endforeach; ?>
</table>

<script type="text/javascript">
    window.print();
</script>