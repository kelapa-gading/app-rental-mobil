<div class="main-content">
    <div class="section">
        <div class="section-header bg-dark">
            <h1>Form Tambah Tipe Mobil</h1>
        </div>
    </div>
    <form method="POST" action="<?php echo base_url('admin/data_tipe/tambah_tipe_aksi') ?>">
        <div class="form-group">
            <label class="text-primary">Kode Tipe</label>
            <input type="text" name="kode_tipe" class="form-control">
            <?= form_error('kode_tipe', '<div class="text-small text-danger">', '</div>') ?>
        </div>
        <div class="form-group">
            <label class="text-primary">Nama Tipe</label>
            <input type="text" name="nama_tipe" class="form-control">
            <?= form_error('nama_tipe', '<div class="text-small text-danger">', '</div>') ?>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
        <button type="reset" class="btn btn-danger">Delete</button>
    </form>
</div>