      <!-- Main Content -->
      <div class="main-content bg-dark">
        <section class="section">
          <div class="section-header bg-light">
            <h1>Dashboard</h1>
          </div>
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 ">
              <div class="card card-statistic-1 bg-secondary">
                <div class="card-icon bg-primary">
                  <i class="fa fa-user"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Total Admin</h4>
                  </div>
                  <div class="card-body">
                    5
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1 bg-secondary">
                <div class="card-icon bg-info">
                  <i class="fa fa-car"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Car Stock</h4>
                  </div>
                  <div class="card-body">
                    42
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1 bg-secondary">
                <div class="card-icon bg-warning">
                  <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Transaction today</h4>
                  </div>
                  <div class="card-body">
                    150
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1 bg-secondary">
                <div class="card-icon bg-success">
                  <i class="fa fa-tasks"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Handled Complaint</h4>
                  </div>
                  <div class="card-body">
                    25
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <div class="card bg-secondary">
                <div class="card-header">
                  <h4>Note</h4>
                </div>
                <div class="card-body">
                  <blockquote class="blockquote mb-0 bg-dark text-light">
                    <p>Lakukan Yang Terbaik hari ini dari hari esok</p>
                    <footer class="blockquote-footer">Jangan Lupa Bersyukur</footer>
                  </blockquote>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
              <div class="card bg-secondary">
                <div class="card-header">
                  <h4>Progress Build Web</h4>
                </div>
                <div class="card-body">
                  <div class="mb-4 mt-4">
                    <div class="text-small float-right font-weight-bold text-muted">95%</div>
                    <div class="font-weight-bold mb-1">Database</div>
                    <div class="progress" data-height="3">
                      <div class="progress-bar" role="progressbar" data-width="95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>

                  <div class="mb-4">
                    <div class="text-small float-right font-weight-bold text-muted">67%</div>
                    <div class="font-weight-bold mb-1">Frontend</div>
                    <div class="progress" data-height="3">
                      <div class="progress-bar" role="progressbar" data-width="67%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>

                  <div class="mb-4">
                    <div class="text-small float-right font-weight-bold text-muted">75%</div>
                    <div class="font-weight-bold mb-1">Feature</div>
                    <div class="progress" data-height="3">
                      <div class="progress-bar" role="progressbar" data-width="75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>

                  <div class="mb-4">
                    <div class="text-small float-right font-weight-bold text-muted">95%</div>
                    <div class="font-weight-bold mb-1">Develop Software</div>
                    <div class="progress" data-height="3">
                      <div class="progress-bar" role="progressbar" data-width="95%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
      </section>
      </div>
      <!-- The core Firebase JS SDK is always required and must be listed first -->
      <script src="/__/firebase/7.15.5/firebase-app.js"></script>

      <!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
      <script src="/__/firebase/7.15.5/firebase-analytics.js"></script>

      <!-- Initialize Firebase -->
      <script src="/__/firebase/init.js"></script>