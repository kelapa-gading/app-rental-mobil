<div class="main-content">
    <div class="section">
        <div class="section-header bg-secondary">
            <h1>Data Tipe Mobil</h1>
        </div>
    </div>
    <a class="btn btn-primary mb-3" href="<?php echo base_url('admin/data_tipe/tambah_tipe') ?>">Edit Tipe Mobil</a>
    <?php echo $this->session->flashdata('pesan') ?>
    <table class="table table-bordered table-striped table-hover table-dark">
        <thead class="thead-dark">
            <tr>
                <th width="20px" scope="col">No</th>
                <th scope="col">Kode Tipe</th>
                <th scope="col">Nama Tipe</th>
                <th width="180px" scope="col">Aksi</th>
            </tr>
        </thead>

        <tbody>

            <?php
            $no = 1;
            foreach ($tipe as $tp) : ?>
                <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $tp->kode_tipe ?></td>
                    <td> <?php echo $tp->nama_tipe ?> </td>
                    <td>
                        <a class="btn btn-sm btn-primary" href="<?php echo base_url('admin/data_tipe/update_tipe/' . $tp->id_tipe) ?>"><i class=" fas fa-edit"></i></a>
                        <a class="btn btn-sm btn-danger" href="<?php echo base_url('admin/data_tipe/delete_tipe/' . $tp->id_tipe) ?>"><i class="fas fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>