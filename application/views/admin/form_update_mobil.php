<div class="main-content">
    <section class="section">
        <div class="section-header bg-dark">
            <h1>Form Update Data mobil</h1>
        </div>
        <div class="card">
            <div class="card-body bg-dark">
                <?php foreach ($mobil as $mb) : ?>

                    <form method="POST" action="<?php echo base_url('admin/data_mobil/update_mobil_aksi') ?>" enctype="multipart/form-data">

                        <div class="row">
                            <div class="col-md6">
                                <div class="form-group">
                                    <label class="text-primary">Tipe Mobil</label>
                                    <input type="hidden" name="id_mobil" value="<?php echo $mb->id_mobil ?>">
                                    <select name="kode_tipe" class="form-control">
                                        <option value="<?php echo $mb->kode_tipe ?>"><?php echo $mb->kode_tipe ?></option>
                                        <?php foreach ($tipe as $tp) : ?>
                                            <option value="<?php echo $tp->kode_tipe ?>">
                                                <?php echo $tp->nama_tipe ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                    <?php echo form_error('kode_tipe', '<div class="text-small text-danger">', '</div>') ?>
                                </div>
                                <div class="form-group mb-n2">
                                    <label class="text-primary">Merk Mobil</label>
                                    <br />
                                    <input type="text" name="merk" class="form-control" value=" <?php echo $mb->merk ?>">
                                </div>
                                <?php echo form_error('merk', '<div class="text-small text-danger">', '</div>') ?>
                                <div class="form-group mb-n2 mt-2">
                                    <label class="text-primary">No. Plat</label>
                                    <br />
                                    <input type="text" name="no_plat" class="form-control" value="<?php echo $mb->no_plat ?>">
                                </div>
                                <?php echo form_error('no_plat', '<div class="text-small text-danger">', '</div>') ?>
                                <div class="form-group mb-n2 mt-2">
                                    <label class="text-primary">Warna</label>
                                    <br />
                                    <input type="text" name="warna" class="form-control" value="<?php echo $mb->warna ?>">
                                </div>
                                <?php echo form_error('warna', '<div class="text-small text-danger">', '</div>') ?>

                                <div class="form-group mt-4">
                                    <label class="text-primary">Fast Charging</label>
                                    <br />
                                    <select name="fast_charging" class="form-control">
                                        <option <?php if ($mb->fast_charging == "1") {
                                                    echo "selected = 'selected'";
                                                }
                                                echo $mb->fast_charging; ?> value="1">Tersedia</option>

                                        <option <?php if ($mb->fast_charging == "0") {
                                                    echo "selected = 'selected'";
                                                }
                                                echo $mb->fast_charging; ?> value="0">Tidak Tersedia</option>


                                    </select>
                                </div>

                                <div class="form-group mt-2">
                                    <label class="text-primary">Free Battery Value</label>
                                    <br />
                                    <select name="free_gass" class="form-control">
                                        <option <?php if ($mb->free_gass == "1") {
                                                    echo "selected = 'selected'";
                                                }
                                                echo $mb->free_gass; ?> value="1">Tersedia</option>

                                        <option <?php if ($mb->free_gass == "0") {
                                                    echo "selected = 'selected'";
                                                }
                                                echo $mb->free_gass; ?> value="0">Tidak Tersedia</option>


                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="text-primary">7 Seater</label>
                                    <br />
                                    <select name="tujuh_kursi" class="form-control">
                                        <option <?php if ($mb->tujuh_kursi == "1") {
                                                    echo "selected = 'selected'";
                                                }
                                                echo $mb->tujuh_kursi; ?> value="1">Tersedia</option>

                                        <option <?php if ($mb->tujuh_kursi == "0") {
                                                    echo "selected = 'selected'";
                                                }
                                                echo $mb->tujuh_kursi; ?> value="0">Tidak Tersedia</option>


                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="text-primary">Automatic</label>
                                    <br />
                                    <select name="matic" class="form-control">
                                        <option <?php if ($mb->matic == "1") {
                                                    echo "selected = 'selected'";
                                                }
                                                echo $mb->matic; ?> value="1">Tersedia</option>

                                        <option <?php if ($mb->matic == "0") {
                                                    echo "selected = 'selected'";
                                                }
                                                echo $mb->matic; ?> value="0">Tidak Tersedia</option>


                                    </select>
                                </div>

                                <div class="form-group mb-n2">
                                    <label class="text-primary">Tahun Pembuatan</label>
                                    <br />
                                    <input type="text" name="tahun" class="form-control" value="<?php echo $mb->tahun ?>">
                                </div>
                                <?php echo form_error('tahun', '<div class="text-small text-danger">', '</div>') ?>

                                <div class="form-group mb-n2 mt-2">
                                    <label class="text-primary">Harga</label>
                                    <br />
                                    <input type="number" name="harga" class="form-control" value="<?php echo $mb->harga ?>">
                                </div>
                                <?php echo form_error('harga', '<div class="text-small text-danger">', '</div>') ?>
                                <div class="form-group mb-n2 mt-2">
                                    <label class="text-primary">Denda</label>
                                    <br />
                                    <input type="number" name="denda" class="form-control" value="<?php echo $mb->denda ?>">
                                </div>
                                <?php echo form_error('denda', '<div class="text-small text-danger">', '</div>') ?>


                                <div class="form-group mt-2">
                                    <label class="text-primary">Status</label>
                                    <br />
                                    <select name="status" class="form-control">
                                        <option <?php if ($mb->status == "1") {
                                                    echo "selected = 'selected'";
                                                }
                                                echo $mb->status; ?> value="1">Tersedia</option>

                                        <option <?php if ($mb->status == "0") {
                                                    echo "selected = 'selected'";
                                                }
                                                echo $mb->status; ?> value="0">Tidak Tersedia</option>


                                    </select>
                                </div>
                                <?php echo form_error('status', '<div class="text-small text-danger">', '</div>') ?>
                                <div class="form-group mb-2">
                                    <label class="text-primary">Gambar</label>
                                    <br />
                                    <input type="file" name="gambar" class="form-control">
                                </div>
                                <div class=" mb-2 pt-2">
                                    <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                                    <button type="reset" class="btn btn-danger ml-2">Hapus</button>
                                </div>
                                <div class="col-md6"></div>
                    </form>
                <?php endforeach; ?>
            </div>
        </div>
</div>
</section>
</div>