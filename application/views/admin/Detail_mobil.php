<div class="main-content">
    <section class="section">
        <div class="section-header bg-dark">
            <h1>Detail mobil</h1>
        </div>
    </section>
    <?php foreach ($detail as $dt) : ?>
        <div class="card bg-dark">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-5">
                        <img src="<?php echo base_url() . 'assets/upload/' . $dt->gambar ?>" width="300px"></div>
                    <div class="col-md-7">
                        <table class="table table-striped table-bordered table-dark">
                            <tr>
                                <td>Tipe Mobil</td>
                                <td> <?php if ($dt->kode_tipe == "CAR01") {
                                            echo "Sedan";
                                        } elseif ($dt->kode_tipe == "CAR02") {
                                            echo "City Car";
                                        } elseif ($dt->kode_tipe == "CAR03") {
                                            echo "Supercar";
                                        } else {
                                            echo "<span class='text-danger'>Tipe Mobil belum terdaftar</span>";
                                        } ?></td>
                            </tr>
                            <tr>
                                <td>Merk</td>
                                <td><?php echo $dt->merk ?></td>
                            </tr>
                            <tr>
                                <td>No.Plat</td>
                                <td><?php echo $dt->no_plat ?></td>
                            </tr>
                            <tr>
                                <td>Warna</td>
                                <td><?php echo $dt->warna ?></td>
                            </tr>
                            <tr>
                                <td>Tahun</td>
                                <td><?php echo $dt->tahun ?></td>
                            </tr>
                            <tr>
                                <td>Harga</td>
                                <td>Rp . <?php echo number_format($dt->harga, 0, ',', '.') ?></td>
                            </tr>
                            <tr>
                                <td>Denda</td>
                                <td>Rp. <?php echo number_format($dt->denda, 0, ',', '.') ?></td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td><?php if ($dt->status == "0") {
                                        echo "<span class='badge badge-danger'>Tidak tersedia </span>";
                                    } else {
                                        echo "<span class='badge badge-primary'>Tersedia </span>";
                                    } ?></td>
                            </tr>
                            <tr>
                                <td>Fast Charge</td>
                                <td><?php if ($dt->fast_charging == "0") {
                                        echo "<span class='badge badge-danger'>Tidak tersedia </span>";
                                    } else {
                                        echo "<span class='badge badge-primary'>Tersedia </span>";
                                    } ?></td>
                            </tr>
                            <tr>
                                <td>Free Battery full power</td>
                                <td><?php if ($dt->free_gass == "0") {
                                        echo "<span class='badge badge-danger'>Tidak tersedia </span>";
                                    } else {
                                        echo "<span class='badge badge-primary'>Tersedia </span>";
                                    } ?></td>
                            </tr>
                            <tr>
                                <td>7 Seater</td>
                                <td><?php if ($dt->tujuh_kursi == "0") {
                                        echo "<span class='badge badge-danger'>Tidak tersedia </span>";
                                    } else {
                                        echo "<span class='badge badge-primary'>Tersedia </span>";
                                    } ?></td>
                            </tr>
                            <tr>
                                <td>Automatic Transmision</td>
                                <td><?php if ($dt->matic == "0") {
                                        echo "<span class='badge badge-danger'>Tidak tersedia </span>";
                                    } else {
                                        echo "<span class='badge badge-primary'>Tersedia </span>";
                                    } ?></td>
                            </tr>
                        </table>
                        <a class="btn btn-sm btn-danger ml-4" href="<?php echo base_url('admin/data_mobil') ?>">Return</a>
                        <a class="btn btn-sm btn-primary" href="<?php echo base_url('admin/data_mobil/update_mobil/' . $dt->id_mobil) ?>">Update</a>
                    </div>
                </div>
            </div>
        </div>

    <?php endforeach; ?>
</div>