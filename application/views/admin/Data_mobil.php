<div class="main-content">
    <section class="section">
        <div class="section-header bg-secondary">
            <h1>Data mobil</h1>
        </div>

        <a href="<?php echo base_url('admin/Data_mobil/tambah_mobil') ?>" class="btn btn-primary mb-3">Tambah Data</a>
        <?php echo $this->session->flashdata('pesan') ?>
        <table class="table table-hover table-striped table-bordered table-dark text-light">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Gambar</th>
                    <th>Tipe</th>
                    <th>Merk</th>
                    <th>No Plat</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($mobil as $mb) :
                ?>
                    <tr>
                        <td> <?php echo $no++ ?></td>
                        <td> <img class="rounded" width="60px" src="<?php echo base_url() . 'assets/upload/' . $mb->gambar ?>"></td>
                        <td> <?php echo $mb->kode_tipe ?></td>
                        <td><?php echo $mb->merk ?> </td>
                        <td> <?php echo $mb->no_plat ?></td>
                        <td> <?php if ($mb->status == "0") {
                                    echo "<span class='badge badge-danger'>Tidak tersedia </span> ";
                                } else {
                                    echo "<span class='badge badge-success'>Tersedia </span> ";
                                }
                                ?> </td>
                        <td>
                            <a href="<?php echo base_url('admin/data_mobil/detail_mobil/') . $mb->id_mobil ?>" class="btn btn-sm btn-success"><i class="fas fa-eye"></i></a>
                            <a href="<?php echo base_url('admin/data_mobil/delete_mobil/') . $mb->id_mobil ?>" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                            <a href=" <?php echo base_url('admin/data_mobil/update_mobil/') . $mb->id_mobil ?>" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?= $pagination; ?>
</div>
</section>
</div>