<div class="main-content">
    <section class="section">
        <div class="section-header bg-secondary">
            <h1>Laporan Transaksi</h1>
        </div>
        <span class="mt-2 p-2"><?php echo $this->session->flashdata('pesan') ?></span>
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-dark">

                <tr>
                    <th>No</th>
                    <th>Customer</th>
                    <th>Mobil</th>
                    <th>Tanggal Sewa</th>
                    <th>Tanggal Kembali</th>
                    <th>Harga Sewa/Hari</th>
                    <th>Jumlah Mobil</th>
                    <th>Denda/Hari</th>
                    <th>Total Denda</th>
                    <th>Tanggal Pengembalian</th>
                    <th>Status Pengembalian</th>
                    <th>Status rental</th>
                    <th>Cek Pembayaran</th>
                    <th>Action</th>
                </tr>

                <?php
                $no = 1;
                foreach ($transaksi as $tr) : ?>
                    <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $tr->nama ?></td>
                        <td><?php echo $tr->merk ?></td>
                        <td><?php echo date('d/m/Y', strtotime($tr->tanggal_sewa)); ?></td>
                        <td><?php echo date('d/m/Y', strtotime($tr->tanggal_kembali)); ?></td>
                        <td>Rp. <?php
                                if (is_numeric($tr->harga) && is_numeric($tr->Jumlah)) {
                                    $sub_total = $tr->harga * $tr->Jumlah;
                                    echo $sub_total;
                                } else {
                                    echo $tr->harga;
                                } ?></td>
                        <td><?php echo $tr->Jumlah ?> Mobil</td>
                        <td>Rp. <?php echo number_format($tr->denda, 0, ',', '.') ?></td>
                        <td>Rp. <?php echo number_format($tr->total_denda, 0, ',', '.') ?></td>
                        <td>
                            <?php if ($tr->tanggal_pengembalian == "0000-00-00") {
                                echo "-";
                            } else {
                                echo date('d/m/Y', strtotime($tr->tanggal_pengembalian));
                            }
                            ?>
                        </td>

                        <td><?php if ($tr->status_pengembalian == "Kembali") {
                                echo "Kembali";
                            } else {
                                echo "Belum Kembali";
                            } ?>
                        </td>

                        <td><?php if ($tr->status_rental == "Sudah Selesai") {
                                echo "Sudah Selesai";
                            } else {
                                echo "Belum Selesai";
                            } ?></td>

                        <td>
                            <center>
                                <?php if (empty($tr->bukti_pembayaran)) { ?>
                                    <button class="btn btn-sm btn-danger"><i class="fas fa-times-circle"></i></button>
                                <?php } else { ?>
                                    <a class="btn btn-sm btn-primary" href="<?php echo base_url('admin/transaksi/pembayaran/' . $tr->id_transaksi) ?>"><i class="fas fa-check-circle"></i></a>
                                <?php } ?>
                            </center>
                        </td>

                        <td>
                            <?php if ($tr->status == "1") {
                                echo "_";
                            } else { ?>
                                <div class="row">
                                    <a class="btn btn-sm btn-success mr-2" href="<?php echo base_url('admin/transaksi/transaksi_selesai/' . $tr->id_transaksi) ?>"> <i class="fas fa-check"></i></a>
                                    <a onclick="return confirm('Apakah Anda Yakin?')" class="btn btn-sm btn-danger" href="<?php echo base_url('admin/transaksi/transaksi_batal/' . $tr->id_transaksi) ?>"> <i class="fas fa-times"></i></a>
                                </div>
                            <?php }  ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </section>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Catatan Penting!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Mohon Maaf,Transaksi Sudah Selesai & Tidak Bisa dibatalkan!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Baik!</button>
            </div>
        </div>
    </div>
</div>