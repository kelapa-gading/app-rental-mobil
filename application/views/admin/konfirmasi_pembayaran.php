<div class="main-content">
    <section class="section">
        <div class="section-header bg-dark">
            <h1>Konfirmasi Pembayaran</h1>
        </div>
        <div class="card" style="width: 55%; background:darkslategrey;">
            <div class="card-header text-light">
                Konfirmasi Pembayaran
            </div>

            <div class="card-body">
                <form method="POST" action="<?php echo base_url('admin/transaksi/cek_pembayaran') ?>">
                    <?php foreach ($pembayaran as $pmb) : ?>
                        <a class="btn btn-sm btn-success" href="<?php echo base_url('admin/transaksi/download_pembayaran/' . $pmb->id_transaksi) ?>"><i class="fas fa-download"></i>Download Bukti Pembayaran</a>
                        <div class="custom-control custom-switch ml-5">
                            <input type="hidden" class="custom-control-input" value="<?php echo $pmb->id_transaksi ?>" name="id_transaksi">
                            <input type="checkbox" class="custom-control-input" id="switch1">
                            <label class="custom-control-label" for="switch1">Konfirmasi Pembayaran</label>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-sm btn-primary" value="1">Simpan</button>
                    <?php endforeach; ?>
                </form>
            </div>
        </div>
    </section>
</div>