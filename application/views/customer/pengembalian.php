<div class="container">
    <div class="card mx-auto" style="margin-top:180px; width:80%;">
        <div class="card-header">
            Pengembalian Mobil
        </div>
        <span class="mt-2 p-2"><?php echo $this->session->flashdata('pesan') ?></span>
        <div class="card-body mb-5">
            <?php foreach ($transaksi as $tr) : ?>
                <form method="POST" action="<?php echo base_url('customer/pengembalian_mobil/aksi_pengembalian_mobil') ?>">
                    <input type="hidden" name="id_transaksi" value="<?php echo $tr->id_transaksi ?>">
                    <input type="hidden" name="Jumlah" value="<?php echo $tr->Jumlah ?>">
                    <input type="hidden" name="tanggal_kembali" value="<?php echo $tr->tanggal_kembali ?>">
                    <input type="hidden" name="denda" value="<?php echo $tr->denda ?>">

                    <div class="form-group">
                        <label>Tanggal Pengembalian</label>
                        <input type="date" name="tanggal_pengembalian" class="form-control" value="<?php echo $tr->tanggal_pengembalian ?>">
                    </div>

                    <div class="form-group">
                        <label> Status Pengembalian</label>
                        <select name="status_pengembalian" class="form-control">
                            <option value="<?php echo $tr->status_pengembalian ?>"><?php echo $tr->status_pengembalian ?></option>
                            <option value="Kembali">Kembali</option>
                            <option value="Belum Kembali">Belum kembali</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label> Status Rental</label>
                        <select name="status_rental" class="form-control">
                            <option value="<?php echo $tr->status_rental ?>"><?php echo $tr->status_rental ?></option>
                            <option value="Sudah Selesai">Selesai</option>
                            <option value="Belum Selesai">Belum Selesai</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-success mb-3">Save</button>

                </form>
            <?php endforeach; ?>
        </div>