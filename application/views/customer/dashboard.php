 <!--== SlideshowBg Area Start ==-->
 <section id="slideslow-bg" style="background:darkslategrey ;">
     <div class="container">
         <div class="row">
             <div class="col-lg-12 text-center">
                 <div class="slideshowcontent">
                     <div class="display-table">
                         <div class="display-table-cell">

                             <a href="<?= base_url('auth/login') ?>" class="btn btn-lg btn-outline-info">Selamat Datang DI Rental Autonet</a>
                             <p class="mt-3">DAPATKAN DISKON 25%<br> UNTUK CUSTOMER LAMA KAMI</p>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section>
 <!--== SlideshowBg Area End ==-->

 <!--== About Us Area Start ==-->
 <section id="about-area" class="section-padding" style="background: slategray;">
     <div class="jumbotron" style="margin-top: -100px; background:slategray;">
         <div class="container">
             <div class="row">
                 <!-- Section Title Start -->
                 <div class="col-lg-12">
                     <div class="section-title  text-center font-weight-bold">
                         <h2>Sekilas Info!</h2>
                         <span class="title-line"><i class="fa fa-car"></i></span>
                         <p>Rental Autonet adalah rental mobil yang menghadirkan inovasi baru untuk beralih ke mobil listrik.</p>
                     </div>
                 </div>
                 <!-- Section Title End -->
             </div>

             <div class="row">
                 <!-- About Content Start -->
                 <div class="col-lg-6">
                     <div class="display-table">
                         <div class="display-table-cell">
                             <div class="about-content">
                                 <p class="font-weight-bold">Rental Sekarang dan Nikmati Pelayanan Kami!.</p>
                             </div>
                         </div>
                     </div>
                 </div>
                 <!-- About Content End -->
                 <!-- carousel  -->
                 <div id="carouselExampleControls" class="carousel slide col-lg-6 mb-5" data-ride="carousel">
                     <div class="carousel-inner">
                         <div class="carousel-item active">
                             <a href="<?= base_url('customer/data_mobil/detail_mobil/7') ?>">
                                 <img src="<?php echo base_url('assets/upload/Selo_electriccar.png') ?>">
                             </a>
                         </div>
                         <div class="carousel-item">
                             <a href="<?= base_url('customer/data_mobil/detail_mobil/6') ?>">
                                 <img src="<?php echo base_url('assets/upload/bmwi3s.jpeg') ?>" class="d-block w-100" width="50%">
                             </a>
                         </div>
                         <div class="carousel-item">
                             <a href="<?= base_url('customer/data_mobil/detail_mobil/9') ?>">
                                 <img src="<?php echo base_url('assets/upload/Toyota_prius.jpeg') ?>" class="d-block w-100" width="50%">
                             </a>
                         </div>
                         <div class="carousel-item">
                             <a href="<?= base_url('customer/data_mobil/detail_mobil/8') ?>">
                                 <img src="<?php echo base_url('assets/upload/miev.jpg') ?>" class="d-block w-100" width="50%">
                             </a>
                         </div>
                         <div class="carousel-item">
                             <a href="<?= base_url('customer/data_mobil/detail_mobil/10') ?>">
                                 <img src="<?php echo base_url('assets/upload/Tesla.jpeg') ?>" class="d-block w-100" width="50%">
                             </a>
                         </div>
                         <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                             <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                             <span class="sr-only">Previous</span>
                         </a>
                         <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                             <span class="carousel-control-next-icon" aria-hidden="true"></span>
                             <span class="sr-only">Next</span>
                         </a>
                     </div>
                 </div>
                 <!-- end carousel -->

             </div>

             <!-- divcard body -->
             <div class="container ">
                 <h1></h1>
                 <div class="row mt-4">
                     <div class="col">
                         <div class="card" style="height: 20rem;">
                             <div class="card-body">
                                 <a href="<?= base_url('customer/data_mobil/detail_mobil/1') ?>">
                                     <img src="<?php echo base_url('assets/upload/molis-hevina1.jpg') ?>" class="card-img-top" style="width: 500px; height:200px ;">
                                     <div class="card-body">
                                         <h2 class="text-info" style="font-family:sans-serif;">Hevina</h2>
                                     </div>
                                 </a>
                             </div>
                         </div>
                     </div>
                     <div class="col">
                         <div class="card" style="height: 20rem;">
                             <div class="card-body">
                                 <a href="<?= base_url('customer/data_mobil/detail_mobil/12') ?>">
                                     <img src="<?php echo base_url('assets/upload/EC-ITS.jpg') ?>" class="card-img-top" style="width: 500px; height:12rem;">
                                     <div class="card-body">
                                         <p class="card-text">
                                             <h2 class="text-info" style="font-family:sans-serif;">E&C ITS</h2>
                                         </p>
                                     </div>
                                 </a>
                             </div>
                         </div>
                     </div>
                     <div class="col">
                         <div class="card">
                             <div class="card-body">
                                 <a href="<?= base_url('customer/data_mobil/detail_mobil/7') ?>">
                                     <img src="<?php echo base_url('assets/upload/Selo_electriccar.png') ?>" class="card-img-top" style="width: 500px;">
                                     <div class="card-body">
                                         <p class="card-text">
                                             <h2 class="text-info" style="font-family: sans-serif;">Selo Electric</h2>
                                         </p>
                                     </div>
                                 </a>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- end div-card body -->
             <!-- About Fretutes Start -->

             <div class="card-body">
                 <div class="row">
                     <!-- Single Fretutes Start -->
                     <div class="col-lg-4">
                         <img style="width: 45%;" src="">
                     </div>
                     <!-- Single Fretutes End -->
                 </div>
             </div>

             <!-- About Fretutes End -->
         </div>
     </div>
 </section>
 <!--== About Us Area End ==-->




 <!-- Section Title Start -->
 <div class="container-fluid">
     <div class="col-lg-12">
         <div class="section-title text-center mt-4">
             <h2>Menuju Halaman Sewa</h2>

         </div>
     </div>
 </div>
 <!-- Section Title End -->
 </div>

 <!-- Service Content Start -->
 <div class="row">
     <div class="col-lg-11 m-auto text-center">

         <!-- Single Service Start -->
         <div class="service-item bg-light">
             <a href="<?= base_url('customer/data_mobil') ?>">
                 <i class="fa fa-taxi"></i>
                 <h3>RENTAL Mobil</h3>
                 <p>Stok Mobil di Rental Kami akan selalu tersedia untuk anda.</p>
             </a>
         </div>
         <!-- Single Service End -->
         <!-- Single Service End -->

     </div>
 </div>
 <!-- Service Content End -->



 <!--== Footer Area Start ==-->
 <section id="footer-area">
     <!-- Footer Widget Start -->
     <div class="footer-widget-area">
         <div class="container">
             <div class="row">
                 <!-- Single Footer Widget Start -->
                 <div class="col-lg-4 col-md-6">
                     <div class="single-footer-widget">
                         <h2>Tentang Kita</h2>
                         <div class="widget-body">
                             <img src="<?php echo base_url() ?>assets/assets_shop/img/logo.jpeg" alt="JSOFT" width="200px">
                             <p>Rental Autonet merupakan usaha perseorangan yang bergerak di bidang otomotif dalam pelayanan sewa mobil electric.</p>
                         </div>
                     </div>
                 </div>
                 <!-- Single Footer Widget End -->
             </div>
         </div>
     </div>
     <!-- Footer Widget End -->
     <!-- The core Firebase JS SDK is always required and must be listed first -->
     <script src="/__/firebase/7.15.5/firebase-app.js"></script>

     <!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
     <script src="/__/firebase/7.15.5/firebase-analytics.js"></script>

     <!-- Initialize Firebase -->
     <script src="/__/firebase/init.js"></script>