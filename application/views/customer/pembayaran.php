<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-md-8">
            <div class="card" style="margin-top: 150px;">
                <div class="card-header alert alert-success">
                    Invoice Pembayaran Anda
                </div>
                <div class="card-body">
                    <table class="table">
                        <?php foreach ($transaksi as $tr) : ?>
                            <tr>
                                <th>Merk Mobil</th>
                                <td>:</td>
                                <td> <?php echo $tr->merk ?> </td>
                            </tr>
                            <tr>
                                <th>Tanggal Sewa</th>
                                <td>:</td>
                                <td> <?php echo $tr->tanggal_sewa ?> </td>
                            </tr>
                            <tr>
                                <th>Tanggal Kembali</th>
                                <td>:</td>
                                <td> <?php echo $tr->tanggal_kembali ?> </td>
                            </tr>
                            <tr>
                                <th>Harga Sewa/Hari</th>
                                <td>:</td>
                                <td>Rp. <?php echo number_format($tr->harga, 0, ',', '.') ?> </td>
                            </tr>
                            <tr>
                                <th>Jumlah Mobil Dipesan</th>
                                <td>:</td>
                                <td><?php echo $tr->Jumlah ?> Mobil </td>
                            </tr>
                            <tr>
                                <?php
                                $H = strtotime($tr->tanggal_kembali);
                                $R = strtotime($tr->tanggal_sewa);
                                $jmlHari = abs(($H - $R) / (60 * 60 * 24));
                                ?>
                                <th>Jumlah Hari</th>
                                <td>:</td>
                                <td> <?php echo $jmlHari ?> Hari </td>
                            </tr>
                            <tr class="text-succes">
                                <th>Jumlah Pembayaran</th>
                                <td>:</td>
                                <td><button class="btn btn-sm btn-primary">Rp. <?php echo number_format($tr->harga * $jmlHari * $tr->Jumlah, 0, ',', '.') ?></button> </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td> <a href="<?php echo base_url('customer/transaksi/cetak_invoice/' . $tr->id_transaksi) ?>" class="btn btn-sm btn-secondary">Print Invoice</a> </td>
                            </tr>

                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card" style="margin-top: 150px;">
                <div class="card-header alert alert-primary">
                    Informasi Pembayaran
                </div>
            </div>
            <div class="card-body">
                <p class="text-warning mb-3">Silahkan Melakukan Pembayaran Melalui No.Rekening Dibawah ini</p>
                <?php foreach ($admin as $adm) : ?>
                    <ul class="list-group">
                        <li class="list-group-item list-group-item-primary"><?= $adm->jenis_bank . " => " . $adm->nama_admin ?></li>
                        <li class="list-group-item list-group-item-info">No Rekening : <?= $adm->no_rekening ?></li>
                    </ul>
                <?php endforeach; ?>
                <?php
                if (empty($tr->bukti_pembayaran)) { ?>
                    <button style="width: 100%" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-file"></i> Upload Bukti Pembayaran</button>
                <?php } elseif ($tr->status_pembayaran == null) { ?>
                    <button style="width: 100%" class="btn btn-sm btn-warning"><i class="fa fa-clock-o mr-1"></i>Menunggu Konfirmasi Pembayaran</button>
                <?php } elseif ($tr->status_pembayaran == '1') { ?>
                    <button style="width:100%" type="button" class="btn btn-sm btn-success mt-3 mr-1"> <i class="fa fa-check-circle mr-1"></i>Pembayaran Selesai</button>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- modal upload bukti pembayaran -->
<div class="modal-fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload Bukti Pembayaran</h5>
                <button type="button" class="close" data-dismis="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="<?php echo base_url('customer/transaksi/aksi_pembayaran') ?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Upload Bukti Pembayaran</label>
                        <input type="hidden" name="id_transaksi" class="form-control" value="<?php echo $tr->id_transaksi ?>">
                        <input type="file" name="bukti_pembayaran" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>