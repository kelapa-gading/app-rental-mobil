<table style="width: 60%;">
    <h2>Invoice Pembayaran Anda</h2>
    <?php foreach ($transaksi as $tr) : ?>

        <tr>
            <td>Nama Customer</td>
            <td>:</td>
            <td><?php echo $tr->nama ?></td>
        </tr>
        <tr>
            <td>Merk Mobil</td>
            <td>:</td>
            <td> <?php echo $tr->merk ?> </td>
        </tr>
        <tr>
            <td>Jumlah Mobil</td>
            <td>:</td>
            <td> <?php echo $tr->Jumlah ?>Mobil</td>
        </tr>
        <tr>
            <td>Tanggal Sewa</td>
            <td>:</td>
            <td> <?php echo $tr->tanggal_sewa ?> </td>
        </tr>
        <tr>
            <td>Tanggal Kembali</td>
            <td>:</td>
            <td> <?php echo $tr->tanggal_kembali ?> </td>
        </tr>
        <tr>
            <td>Harga Sewa/Hari</td>
            <td>:</td>
            <td>Rp. <?php echo number_format($tr->harga * $tr->Jumlah, 0, ',', '.') ?> </td>
        </tr>
        <tr>
            <?php
            $H = strtotime($tr->tanggal_kembali);
            $R = strtotime($tr->tanggal_sewa);
            $jmlHari = abs(($H - $R) / (60 * 60 * 24));
            ?>
            <td>Jumlah Hari</td>
            <td>:</td>
            <td> <?php echo $jmlHari ?> Hari </td>
        </tr>

        <tr>
            <td>Status Pembayaran</td>
            <td>:</td>
            <td> <?php if ($tr->status_pembayaran == 0) {
                        echo "Belum Lunas";
                    } else {
                        echo "Lunas";
                    } ?></td>
        </tr>

        <tr style="font-weight: bold; color:brown;">
            <td>Jumlah Pembayaran</td>
            <td>:</td>
            <td>Rp. <?php echo number_format($tr->harga * $jmlHari, 0, ',', '.') ?></td>
        </tr>

        <tr>
            <td>Rekening Pembayaran</td>
            <td>:</td>
            <td>
                <ul>
                    <li>Bank Mandiri 701 500 554 8</li>
                    <li>Bank Bca 0650 8284 96</li>
                    <li>Bank BNI 221 220 713 5</li>
                </ul>
            </td>
        </tr>

    <?php endforeach; ?>
</table>

<script type="text/javascript">
    window.print();
</script>