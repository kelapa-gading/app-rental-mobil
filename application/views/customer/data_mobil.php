    <!--== Page Title Area Start ==-->
    <section id="page-title-area" class="section-padding overlay">
        <div class="container">
            <div class="row">
                <!-- Page Title Start -->
                <div class="col-lg-12">
                    <div class="section-title  text-center">
                        <h2>Pilih Mobil</h2>
                        <span class="title-line"><i class="fa fa-car"></i></span>
                        <p>Pilih Mobil yang diminati serta nikmati beberapa fiturnya.</p>
                    </div>
                </div>
                <!-- Page Title End -->
            </div>
        </div>
    </section>
    <!--== Page Title Area End ==-->

    <!--== Car List Area Start ==-->

    <section id="car-list-area" class="section-padding">
        <div class="container">
            <?php echo $this->session->flashdata('pesan') ?>
            <div class="row">
                <!-- Car List Content Start -->
                <div class="col-lg-8">
                    <div class="car-list-content">
                        <div class="row">
                            <?php

                            foreach ($mobil as $mb) : ?>
                                <!-- Single Car Start -->
                                <div class="single-car-wrap">
                                    <div class="row">
                                        <!-- Single Car Thumbnail -->
                                        <div class="col-lg-5">
                                            <img src="<?php echo base_url('assets/upload/' . $mb->gambar) ?>" style="width: 485px;">
                                        </div>
                                        <!-- Single Car Thumbnail -->

                                        <!-- Single Car Info -->
                                        <div class="col-lg-7">
                                            <div class="display-table">
                                                <div class="display-table-cell">
                                                    <div class="car-list-info">
                                                        <h2><a href="<?php echo base_url('customer') ?>"><?php echo $mb->merk ?></a></h2>
                                                        <h5>Rp.<?php echo number_format($mb->harga, 0, ',', '.') ?> /Hari</h5>
                                                        <ul class="car-info-list">
                                                            <li><?php if ($mb->fast_charging == "1") {
                                                                    echo "<i class='fa fa-check-circle text-primary'></i>";
                                                                } else {
                                                                    echo  "<i class='fa fa-times text-danger'></i>";
                                                                }
                                                                ?>Pengisian Daya Cepat</li>
                                                            <li><?php if ($mb->free_gass == "1") {
                                                                    echo "<i class='fa fa-check-circle text-primary'></i>";
                                                                } else {
                                                                    echo  "<i class='fa fa-times text-danger'></i>";
                                                                }
                                                                ?>Gratis Daya Baterai</li>
                                                            <li><?php if ($mb->matic == "1") {
                                                                    echo "<i class='fa fa-check-circle text-primary'></i>";
                                                                } else {
                                                                    echo  "<i class='fa fa-times text-danger'></i>";
                                                                }
                                                                ?>Automatis Transmisi</li>
                                                            <li><?php if ($mb->tujuh_kursi == "1") {
                                                                    echo "<i class='fa fa-check-circle text-primary'></i>";
                                                                } else {
                                                                    echo  "<i class='fa fa-times text-danger'></i>";
                                                                }
                                                                ?>7 Penumpang</li>
                                                        </ul>

                                                        <?php echo anchor('customer/rental/tambah_rental/' . $mb->id_mobil, '<span class="rent-btn">Rental</span>'); ?>

                                                        <a href="<?php echo base_url('customer/data_mobil/detail_mobil/' . $mb->id_mobil) ?>" class="rent-btn">Detail Mobil</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Single Car info -->
                                    </div>
                                </div>
                                <!-- Single Car End -->
                            <?php endforeach; ?>

                        </div>
                    </div>
                </div>
                <!-- Car List Content End -->

                <!-- Sidebar Area Start -->
                <div class="col-lg-4">
                    <div class="sidebar-content-wrap m-t-50">
                        <!-- Single Sidebar Start -->
                        <div class="single-sidebar">
                            <h3><i class="fa fa-info-circle"></i> Untuk Informasi Lebih lanjut</h3>

                            <div class="sidebar-body">
                                <p><i class="fa fa-mobile"></i>+6281218962230</p>
                                <p><i class="fa fa-clock-o"></i><?php

                                                                date_default_timezone_set("Asia/Jakarta");

                                                                $b = time();
                                                                $hour = date("G", $b);

                                                                if ($hour >= 0 && $hour <= 11) {
                                                                    echo "Selamat Pagi :)";
                                                                } elseif ($hour >= 12 && $hour <= 14) {
                                                                    echo "Selamat Siang :) ";
                                                                } elseif ($hour >= 15 && $hour <= 17) {
                                                                    echo "Selamat Sore :) ";
                                                                } elseif ($hour >= 17 && $hour <= 18) {
                                                                    echo "Selamat Petang :) ";
                                                                } elseif ($hour >= 19 && $hour <= 23) {
                                                                    echo "Selamat Malam :) ";
                                                                }

                                                                ?></p>
                            </div>
                        </div>
                        <!-- Single Sidebar End -->


                        <!-- Single Sidebar Start -->
                        <div class="single-sidebar">
                            <h3><i></i>Sosial Media</h3>

                            <div class="sidebar-body">
                                <div class="social-icons text-center">
                                    <a href="https://web.facebook.com/ghalieeunhyuk.ttaesung" target="_blank"><i class="fa fa-facebook"></i></a>
                                    <a href="https://www.instagram.com/challenge/?next=/mochammad_ghalie/" target="_blank"><i class="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Single Sidebar End -->
                    </div>
                </div>
                <!-- Sidebar Area End -->
            </div>
            <div class="row mt-4">
                <div class="col">
                    <?= $pagination; ?>
                </div>
            </div>
        </div>
    </section>
    <!--== Car List Area End ==-->


    <!--== Footer Area Start ==-->
    <section id="footer-area">
        <!-- Footer Widget Start -->
        <div class="footer-widget-area">
            <div class="container">
                <div class="row">
                    <!-- Single Footer Widget Start -->
                    <div class="col-lg-4 col-md-6">
                        <div class="single-footer-widget">
                            <h2>About Us</h2>
                            <div class="widget-body">
                                <img src="<?php echo base_url() ?>assets/assets_shop/img/logo.jpeg" alt="JSOFT" width="50%">
                                <p>Rental Autonet merupakan usaha perseorangan yang bergerak di bidang otomotif dalam pelayanan sewa mobil electric.</p>
                            </div>
                        </div>
                    </div>
                    <!-- Footer Widget End -->