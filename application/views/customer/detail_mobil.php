<div class="container mt-5 mb-5">
    <div class="card" style="margin-top: 200px;">
        <div class="card-body">
            <?php foreach ($detail as $dt) : ?>
                <div class="row">
                    <div class=" col-md-6">
                        <img style="width: 90%;" src="<?php echo base_url('assets/upload/' . $dt->gambar) ?>">
                    </div>
                    <div class="col-md-6">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>Merk</th>
                                <td><?php echo $dt->merk ?></td>
                            </tr>

                            <tr>
                                <th>Warna</th>
                                <td><?php echo $dt->warna ?></td>
                            </tr>
                            <tr>
                                <th>Tahun Produksi</th>
                                <td><?php echo $dt->tahun ?></td>
                            </tr>
                            <tr>
                                <th>Transmisi Otomatis</th>
                                <td><?php
                                    if ($dt->matic == '1') {
                                        echo "<span class='badge badge-success'>Tersedia </span>";
                                    } else {
                                        echo "<span class='badge badge-danger'>Manual Transmisi </span>";
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Pengisian Daya Cepat</th>
                                <td><?php
                                    if ($dt->fast_charging == '1') {
                                        echo "<span class='badge badge-success'>Tersedia </span>";
                                    } else {
                                        echo "<span class='badge badge-danger'>Pengisian waktu standar </span>";
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td><?php if ($dt->status == '1') {
                                        echo "<span class='btn btn-sm btn-outline-success'>Tersedia</span>";
                                    } else {
                                        echo "Telah Dirental";
                                    }
                                    ?></td>
                            <tr>
                                <td colspan="2">
                                    <?php if ($dt->status === "0") {
                                        echo "<span class='btn btn-danger' disable>Telah Dirental</span>";
                                    } else {
                                        echo anchor('customer/rental/tambah_rental/' . $dt->id_mobil, '<button class="btn btn-primary">Rental</button>');
                                    } ?>
                                </td>
                            <?php endforeach; ?>
                            </tr>
                            </tr>
                        </table>
                    </div>
                </div>
        </div>
    </div>
</div>