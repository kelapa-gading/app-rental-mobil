<div class="container">
    <div class="card" style="margin-top: 200px; margin-bottom:50px;">
        <div class="card-header">
            Form Rental Mobil
        </div>

        <div class="card-body">
            <?php
            if ((is_array($detail)) || !empty($value)) {
                foreach ($detail as $dt => $value) { ?>
                    <form method="POST" action="<?php echo base_url('customer/rental/aksi_tambah_rental') ?>">
                        <div class="form-group">
                            <label>Harga Sewa/Hari</label>
                            <input type="hidden" name="id_mobil" value="<?php echo $value->id_mobil ?>">
                            <input type="text" name="harga" class="form-control" value="<?php echo $value->harga ?>" readonly>
                        </div>

                        <div class="form-group">
                            <label>Denda/Hari</label>
                            <input type="text" name="denda" class="form-control" value="<?php echo $value->denda ?>" readonly>
                        </div>

                        <div class="form-group">
                            <label>Jumlah Mobil</label>
                            <select name="Jumlah" id="jumlah" class="form-control">
                                <option value="1">1 Mobil</option>
                                <option value="2">2 Mobil</option>
                                <option value="3">3 Mobil</option>
                                <option value="4">4 Mobil</option>
                                <option value="5">5 Mobil</option>
                                <option value="6">6 Mobil</option>
                                <option value="7">7 Mobil</option>
                                <option value="8">8 Mobil</option>
                                <option value="9">9 Mobil</option>
                                <option value="10">10 Mobil</option>
                                <option value="11">11 Mobil</option>
                                <option value="12">12 Mobil</option>
                                <option value="13">13 Mobil</option>
                                <option value="14">14 Mobil</option>
                                <option value="15">15 Mobil</option>

                            </select>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Rental</label>
                            <input type="date" name="tanggal_sewa" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Tanggal Kembali</label>
                            <input type="date" name="tanggal_kembali" class="form-control">
                        </div>

                        <button type="submit" class="btn btn-primary">Rent</button>
                    </form>
            <?php }
            } ?>
        </div>
    </div>
</div>