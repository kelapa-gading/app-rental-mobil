<body>
  <div id="app">
    <section class="section">
      <div class="d-flex flex-wrap align-items-stretch">
        <div class="col-lg-4 col-md-6 col-12 order-lg-1 min-vh-100 order-2 bg-white">
          <div class="p-4 m-3">
            <img src="<?= base_url('assets/assets-stisla') ?>/assets/img/Autonet.svg" alt="logo" width="80" class="shadow-light rounded-circle mb-5 mt-2">
            <h4 class="text-dark font-weight-normal">Selamat Datang Di <span class="font-weight-bold">Rental_Autonet</span></h4>
            <p class="text-muted">Ganti Password atau reset password.</p>
            <span class="m-2"><?php echo $this->session->flashdata('pesan') ?></span>
            <form method="POST" action="<?php echo base_url('auth/aksi_ganti_password') ?>">
              <span clas="m-2"><?php echo $this->session->flashdata('pesan') ?></span>
              <div class="form-group">
                <label for="pass_baru">Password Baru</label>
                <input id="pass_baru" type="password" class="form-control" name="pass_baru" tabindex="1" autofocus>
                <?php echo form_error('pass_baru', '<div class="text-danger text-small">', '</div>') ?>
                <div class="form-group">
                  <div class="d-block">
                    <label for="ulangi_pass" class="control-label">Ulangi Password Kamu</label>
                  </div>
                  <input id="ulangi_pass" type="password" class="form-control" name="ulangi_pass" tabindex="2">
                  <?php echo form_error('ulangi_pass', '<div class="text-danger text-small">', '</div>') ?>

                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-icon icon-right" tabindex="4">
                  YUP GANTI PASSWORD!
                </button>
              </div>
            </form>
          </div>
        </div>
        <div class="col-lg-8 col-12 order-lg-2 order-1 min-vh-100 background-walk-y position-relative overlay-gradient-bottom" data-background="<?php echo base_url('assets/assets-stisla') ?>/assets/img/unsplash/login-bg.jpg">
          <div class="absolute-bottom-left index-2">
            <div class="text-primary p-5 pb-2">
              <div class="mb-5 pb-3">
                <h1 class="mb-2 display-4 font-weight-bold">
                  <?php

                  date_default_timezone_set("Asia/Jakarta");

                  $b = time();
                  $hour = date("G", $b);

                  if ($hour >= 0 && $hour <= 11) {
                    echo "Selamat Pagi :)";
                  } elseif ($hour >= 12 && $hour <= 14) {
                    echo "Selamat Siang :) ";
                  } elseif ($hour >= 15 && $hour <= 17) {
                    echo "Selamat Sore :) ";
                  } elseif ($hour >= 17 && $hour <= 18) {
                    echo "Selamat Petang :) ";
                  } elseif ($hour >= 19 && $hour <= 23) {
                    echo "Selamat Malam :) ";
                  }

                  ?></h1>
                <h5 class="font-weight-normal text-muted-transparent">Jakarta, Indonesia<br><?php echo date('H:i T') ?></br></h5>
              </div>
              Photo by <a class="text-light bb" target="_blank" href="https://unsplash.com/photos/a8lTjWJJgLA">Justin Kauffman</a> on <a class="text-light bb" target="_blank" href="https://unsplash.com">Unsplash</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>