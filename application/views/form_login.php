<body class="loader-active">

  <!--== Preloader Area Start ==-->
  <div class="preloader">
    <div class="preloader-spinner">
      <div class="loader-content">
        <img src="<?= base_url('assets/assets_shop/img/preloader.gif') ?>" alt="JSOFT">
      </div>
    </div>
  </div>
  <!--== Preloader Area End ==-->

  <!--== Page Title Area Start ==-->
  <section id="page-title-area" class="section-padding overlay">
    <div class="container">
      <div class="row">
        <!-- Page Title Start -->
        <div class="col-lg-12">
          <div class="section-title  text-center">
            <h2>Login</h2>
            <span class="title-line"><i class="fa fa-car"></i></span>
            <p>Login Terlebih Dahulu Sebelum Masuk Rental Kami.</p>
          </div>
        </div>
        <!-- Page Title End -->
      </div>
    </div>
  </section>
  <!--== Page Title Area End ==-->

  <!--== Login Page Content Start ==-->
  <section id="lgoin-page-wrap" class="section-padding">
    <div class="container">
      <?= $this->session->flashdata('pesan') ?>
      <div class="row">
        <div class="col-lg-6 col-md-8 m-auto">
          <div class="login-page-content">
            <div class="login-form">
              <h3>Hai Selamat Datang Kembali!</h3>
              <form method="POST" action="<?= base_url('auth/login') ?>" class="form-group">
                <div class="username">
                  <input type="text" placeholder="Username Anda" name="username">
                </div>
                <div class="password">
                  <input type="password" placeholder="Password" name="password">
                </div>
                <div class="log-btn">
                  <button type="submit"><i class="fa fa-sign-in"></i> Log In</button>
                </div>
              </form>
            </div>


            <div class="create-ac">
              <p>Belum Punya akun? <a href="<?= base_url('register') ?>">Daftar Dulu</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--== Login Page Content End ==-->

  <!--== Footer Area Start ==-->
  <section id="footer-area">
    <!-- Footer Widget Start -->
    <div class="footer-widget-area">
      <div class="container">
        <div class="row">
          <!-- Single Footer Widget Start -->
          <div class="col-lg-4 col-md-6">
            <div class="single-footer-widget">
              <h2>Tentang Kita</h2>
              <div class="widget-body">
                <img src="<?php echo base_url() ?>assets/assets_shop/img/logo.jpeg" alt="JSOFT" width="200px">
                <p>Rental Autonet merupakan usaha perseorangan yang bergerak di bidang otomotif dalam pelayanan sewa mobil electric.</p>
              </div>
            </div>
          </div>
          <!-- Single Footer Widget End -->
        </div>
      </div>
    </div>